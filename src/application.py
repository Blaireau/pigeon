#!/usr/bin/env python3

# This file is part of pigeon, a keyboard-driven configurable email client.
# Copyright (C) 2022  Baptiste Lambert (Blaireau) rabbitstemplate@disroot.org
# 
# Pigeon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from kivy.app import App
from kivy.core.window import Window
from kivy.core.audio import SoundLoader
from kivy.clock import Clock
from kivy.uix.popup import Popup
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.properties import (ObjectProperty, StringProperty, BooleanProperty,
                             NumericProperty, BoundedNumericProperty,
                             ListProperty)

from math import floor

from configmanager import ConfigManager, InvalidConfig
from commandmanager import CommandManager, CommandError
from constants import (USER_CONFIG_FILE, APP_TITLE, APP_ICON, RESEND_KEY_DELAY,
                       PANELS)
from mailmanager import MailManager
from systemtools import fix_wm_class
import panels


command = CommandManager()


class PigeonApp(App):

    def __init__(self, command_manager: CommandManager):
        super().__init__(title=APP_TITLE, icon=APP_ICON)
        # fix class name
        fix_wm_class('pigeon')
        # keyboard setup
        self._keyboard = Window.request_keyboard(self._keyboard_closed, self.root, 'text')
        self._keyboard.bind(on_key_down=self._on_keyboard_down)
        self.send_keys = True
        self.command_manager = command_manager
        # error management
        self.error_popup : None | Popup = None
        self.fatal_error: bool = False
        # read user config
        try:
            self.configs = ConfigManager(USER_CONFIG_FILE)
            keymap = self.configs.get_merged_dict('keys')
            # set keymap
            self.command_manager.keymap = keymap
        except (InvalidConfig, CommandError) as error:
            message = f'{str(error)}\nFix the invalid config and restart pigeon'
            self.launch_error('Invalid config', message, fatal=True)
        else:
            # mail manager
            self.mail_manager = MailManager()

    def launch_error(self, title: str, error_message: str, fatal: bool = False):
        self.error_popup = Popup(title=title,
                                 content=Label(text=error_message),
                                 size_hint=(1, None),
                                 height=200)
        self.fatal_error = fatal
        self.error_popup.open()

    def _keyboard_closed(self):
        pass

    def _on_keyboard_down(self, keyboard, keycode, text, modifiers):
        if not self.send_keys:
            return True
        if self.error_popup is not None:
            if keycode[1] != 'enter':
                self.error_popup.dismiss()
                self.error_popup = None
                if self.fatal_error:
                    self.quit()
            return
        self.command_manager.on_key_down(self, keycode, modifiers)
        # Return True to accept the key. Otherwise, it will be used by the system.
        return True

    def on_focus(self, window, focus):
        if focus:
            Clock.schedule_once(self.reactivate_keys, RESEND_KEY_DELAY)
        else:
            self.send_keys = False

    def reactivate_keys(self, *args):
        self.send_keys = True

    @command('ping')
    def ping(self):
        raise CommandError('pong')

    @command('debug')
    def debug(self):
        print('rien')

    @command('quit')
    def quit_app(self):
        self.root.email_list_panel.clear()
        self.stop()

    def refresh_email_list(self):
        emails = self.mail_manager.get_inbox()
        self.root.email_list_panel.rebuild(emails)

    @command('move-focus',
             (lambda x: x in ['up', 'down'],
              "one of 'up', 'down'")
            )
    def move_focus(self, direction):
        self.root.panel_manager.current_screen.move_focus(direction)

    @command('current',
             (lambda x: x in ['show'], "one on 'show'")
            )
    def action_on_current(self, action):
        self.root.panel_manager.current_screen.action_on_current(self, action)

    @command('cycle-panel')
    def cycle_panel(self):
        old_panel = self.root.panel_manager.current
        new_index = (PANELS.index(old_panel) + 1) % len(PANELS)
        self.root.panel_manager.current = PANELS[new_index]

    @command('show-panel', (lambda p: p in PANELS, f"one of {', '.join(PANELS)}"))
    def show_panel(self, panel):
        self.root.panel_manager.current = panel

    @command('enter-command')
    def enter_command(self):
        self.root.command_bar.focus = True

    def show_email(self, email):
        if email is None:
            return
        self.root.email_view_panel.email = email
        self.root.panel_manager.current = 'email-view'

    def on_command_mode(self, textinput, focused):
        if focused:
            self.command_manager.command_mode = True
        else:
            Clock.schedule_once(self.exit_command_mode)

    def exit_command_mode(self, *args):
        self.command_manager.command_mode = False

    def execute_command(self, command_line: str):
        try:
            self.command_manager.interprete(self, command_line)
        except CommandError as error:
            self.launch_error('Invalid command', str(error))
        self.root.command_bar.text = ''

    def on_start(self):
        self.refresh_email_list()
        self.root.command_bar.bind(focus=self.on_command_mode)
        self.root_window.bind(focus=self.on_focus)
