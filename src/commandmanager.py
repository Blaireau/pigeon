#!/usr/bin/env python3

# This file is part of maestro, a keyboard-driven configurable music player.
# Copyright (C) 2022  Baptiste Lambert (Blaireau) rabbitstemplate@disroot.org
# 
# Maestro is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import Callable

ExpectationType = tuple[Callable, str]


class CommandError(Exception):
    pass

class CommandManager:

    def __init__(self):
        self.command_mode = False
        self._commands : dict[str, Callable] = {}
        self._args_specs : dict[str, tuple[ExpectationType, ...]] = {}
        self._keymap : dict[str, Callable] = {}
        self._modifiers = []

    def __call__(self, name: str, *args: ExpectationType) -> Callable:
        def decorator(func: Callable) -> Callable:
            # def wrap(*args, **kwargs):
            #     return func(*args, **kwargs)
            self._commands[name] = func
            self._args_specs[name] = args
            return func
        return decorator

    def check_args_specs(self, name: str, args: list[str]):
        specs = self._args_specs[name]
        expected_size = len(specs)
        given_size = len(args)
        if expected_size != given_size:
            raise CommandError(f'Command {name} takes {expected_size} arguments'
                               f' but {given_size} where given.')
        for arg_value, expectations in zip(args, specs):
            check, info_message = expectations
            if not check(arg_value):
                raise CommandError(f'Invalid argument {arg_value} for command {name};'
                                   f' should be {info_message}')



    def interprete(self, app, command_line: str):
        command = self.parse_command(command_line)
        self.run_command(app, command)

    def parse_command(self, command_line: str) -> Callable:
        name, *args = command_line.split(' ')
        try:
            command = self._commands[name]
        except KeyError:
            raise CommandError(f'Command {name} does not exist')
        self.check_args_specs(name, args)
        return lambda app: command(app, *args)

    def run_command(self, app, command: Callable):
        command(app)

    def on_key_down(self, app, keycode, modifiers):
        self._modifiers = modifiers
        key_id, key_str = keycode
        key_name = '-'.join([*self._modifiers, key_str])
        if self.command_mode:
            return
        try:
            command = self._keymap[key_str]
        except KeyError:
            return
        self.run_command(app, command)

    @property
    def keymap(self):
        return self._keymap

    @keymap.setter
    def keymap(self, newkeymap):
        for key, value in newkeymap.items():
            command = self.parse_command(value)
            self._keymap[key] = command
