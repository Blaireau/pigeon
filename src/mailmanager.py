import subprocess
import json


class Email:

    def __init__(self, data):
        self._thread = data['thread']
        self._timestamp = data['timestamp']
        self.subject = data['subject']
        self.author = data['authors']
        self.tags = data['tags']

    def get_full_email_data(self):
        cmd = ['notmuch', 'show', '--format=json', self._thread]
        res = subprocess.run(cmd, capture_output=True)
        data = json.loads(res.stdout)
        self.body = self.extract_body(data)
        return self.body

    def extract_body(self, data):
        thread, *_ = data
        message, *_ = thread
        message_data, *_ = message
        body_data = message_data['body']
        multipart = body_data[0]['content']
        for part in multipart:
            if part['content-type'] == 'text/plain':
                return part['content']
        return f'email has no plain text version'


    def __repr__(self):
        return f'Email[ {self.author[:20]:20} | {self.subject[:50]:50} ]'


class MailManager:

    def get_inbox(self):
        cmd = ['notmuch', 'search', '--format=json', 'tag:inbox']
        res = subprocess.run(cmd, capture_output=True)
        data = json.loads(res.stdout)
        mails = [Email(mail_data) for mail_data in data]
        return mails




if __name__ == '__main__':
    mgr = MailManager()
    data = mgr.get_inbox()
    mail = data[27]
    md = mail.get_full_email_data()
    breakpoint()
