#!/usr/bin/env python3

# This file is part of pigeon, a keyboard-driven configurable email client.
# Copyright (C) 2022  Baptiste Lambert (Blaireau) rabbitstemplate@disroot.org
# 
# Pigeon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from kivy.uix.boxlayout import BoxLayout
from kivy.uix.screenmanager import Screen
from kivy.uix.widget import Widget
from kivy.uix.label import Label
from kivy.uix.image import Image
from kivy.factory import Factory
from kivy.properties import (ObjectProperty, StringProperty, BooleanProperty,
                             NumericProperty, BoundedNumericProperty,
                             ListProperty, AliasProperty)
from kivy.utils import escape_markup

from enum import Flag

from utils import (clamp, add_links_markup, remove_alt_text, remove_alt_images,
                   remove_too_much_newlines)
from constants import EMAIL_LINE_HEIGHT


class ItemState(Flag):
    NORMAL = 0
    FOCUSED = 1


class ListItemPointer:

    def __init__(self, value=None, length: int =0):
        assert isinstance(length, int)
        self._length = length
        if value is None or self._length == 0:
            self._value = None
        else:
            self._value = clamp(0, self._length - 1, value)

    def __repr__(self):
        return f'ptr: {self._value} | {self._length}'

    def get_item_from(self, list_data: list):
        if self._value is None or self._length == 0:
            return None
        return list_data[self._value]

    def get_index(self):
        if self._value is None or self._length == 0:
            return None
        return self._value

    def increment_with_stop(self, increment: int, loop: bool):
        if self._length == 0:
            return ListItemPointer(None, 0)
        if self._value is None:
            if increment > 0:
                new_value = 0
            else:
                new_value = self._length - 1
        else:
            new_value = self._value + increment
            if loop:
                new_value = new_value % self._length
        if 0 <= new_value < self._length:
            return ListItemPointer(new_value, self._length)
        return ListItemPointer(None, self._length)

    def __add__(self, increment: int):
        if self._value is None:
            return ListItemPointer(None, self._length)
        new_value = clamp(0, self._length - 1, self._value + increment)
        return ListItemPointer(new_value, self._length)

    def __sub__(self, increment: int):
        if self._value is None:
            return ListItemPointer(None, self._length)
        new_value = clamp(0, self._length - 1, self._value - increment)
        return ListItemPointer(new_value, self._length)


class BackgroundColor(Widget):
    pass


class EmailLine(BackgroundColor, BoxLayout):
    index = NumericProperty(-1)
    state = ObjectProperty(ItemState.NORMAL)
    email = ObjectProperty(None)

    def on_state(self, _, new_state):
        if self.state == ItemState.FOCUSED:
            self.background_color = (0, 0.2, 0.8)
        else:
            self.background_color = (0, 0, 0)

    def on_focus_ptr(self, parent_screen, focus_ptr):
        if self.index == focus_ptr.get_index():
            self.state |= ItemState.FOCUSED
            parent_screen.scroll_view.scroll_to(self)
        else:
            self.state &= ~ItemState.FOCUSED


class Panel(Screen):
    focus_ptr = ObjectProperty(ListItemPointer())
    data_list = ListProperty()
    has2DMotion = BooleanProperty(False)

    def clear(self):
        self.central_panel.clear_widgets()
        self.data_list = []

    def rebuild(self, data: list):
        self.central_panel.clear_widgets()
        for index, data_item in enumerate(data):
            self._add_item(index, data_item)
        self.focus_ptr = ListItemPointer(0, len(data))
        self.data_list = data

    def on_data_list(self, _, data_list):
        self.central_panel.itemcount = len(data_list)
        ptr_index = self.focus_ptr.get_index()
        if ptr_index is None and len(data_list) > 0:
            ptr_index = 0
        self.focus_ptr = ListItemPointer(ptr_index, len(data_list))

    def add_item(self, index: int, data_item):
        item = self._add_item(index, data_item)
        self.data_list.append(data_item)

    def _add_item(self, index: int, data_item):
        item = self.build_panel_item(data_item)
        item.index = index
        self.bind(focus_ptr=item.on_focus_ptr)
        self.central_panel.add_widget(item)
        return item

    def move_focus(self, motion: str):
        moves = {'up': -1,
                 'down': 1}
        self.focus_ptr += moves[motion]



class EmailListPanel(Panel):

    def build_panel_item(self, email):
        # warning: index is not yet set on item
        item = EmailLine(email=email, height=EMAIL_LINE_HEIGHT)
        return item

    def action_on_current(self, app, action: str):
        match action:
            case 'show':
                mail = self.focus_ptr.get_item_from(self.data_list)
                app.show_email(mail)


class EmailViewPanel(Panel):
    email = ObjectProperty(None)
    subject = StringProperty()
    author = StringProperty()
    body = StringProperty()

    def on_email(self, screen, email):
        email.get_full_email_data()
        self.subject = f'Subject: {email.subject}'
        self.author = f'From: {email.author}'
        body_text = email.body
        body_text = remove_alt_text(body_text)
        body_text = remove_alt_images(body_text)
        body_text = escape_markup(body_text)
        body_text = add_links_markup(body_text)
        body_text = remove_too_much_newlines(body_text)
        self.body = body_text


class EmailBody(Label):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.bind(on_ref_press=print)



Factory.register('BackgroundColor', BackgroundColor)
Factory.register('EmailLine', EmailLine)
Factory.register('EmailListPanel', EmailListPanel)
Factory.register('EmailViewPanel', EmailViewPanel)
Factory.register('EmailBody', EmailBody)
