#!/usr/bin/env python3

# This file is part of maestro, a keyboard-driven configurable music player.
# Copyright (C) 2022  Baptiste Lambert (Blaireau) rabbitstemplate@disroot.org
# 
# Maestro is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import re


def clamp(minimum: int, maximum: int, value: int) -> int:
    return max(minimum, min(maximum, value))

def format_time(total_ms: int) -> str:
    total_seconds = round(total_ms / 1000)
    minutes = total_seconds // 60
    seconds = total_seconds % 60
    return f'{minutes:02}:{seconds:02}'

def remove_alt_text(text: str) -> str:
    alt = r'\s*?\[[\w\s]*\]\s*?\n'
    out = re.sub(alt, '', text)
    return out

def remove_alt_images(text: str) -> str:
    alt = r'\s*?\[\s*http\S*\.(jpg|png|jpeg)\]\s*?\n'
    out = re.sub(alt, '', text)
    return out

def remove_too_much_newlines(text: str) -> str:
    too_much = r'(\n\n\n)\n*'
    out = re.sub(too_much, r'\1', text)
    return out


def add_links_markup(text: str) -> str:
    # replace link tags of this format:
    # 'here <https://example.com/join>' 
    links = r'(http\S+)|(\s\S+)\s*<(.*?)>'

    def replace(link_match):
        simple_link = link_match.group(1)
        if simple_link: # simple links
            ref = simple_link
            link_text = simple_link
        else: # links with tag
            ref = link_match.group(3)
            link_text = link_match.group(2)
        return f'[b][ref={ref}]{link_text}[/ref][/b]'

    out = re.sub(links, replace, text)
    return out
