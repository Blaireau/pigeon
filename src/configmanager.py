#!/usr/bin/env python3

# This file is part of pigeon, a keyboard-driven configurable email client.
# Copyright (C) 2022  Baptiste Lambert (Blaireau) rabbitstemplate@disroot.org
# 
# Pigeon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import yaml # type: ignore

from typing import Any, TypeVar
from pathlib import Path
from constants import DEFAULT_CONFIG_FILE


DatabaseType = dict[str, Any]
ConfigValueType = TypeVar('ConfigValueType', str, bool, DatabaseType)

class InvalidConfig(Exception):
    pass


class ConfigManager:

    def __init__(self, configfile: str):
        self._default = self.read_config_file(DEFAULT_CONFIG_FILE)
        path = Path(configfile).expanduser()
        try:
            self._data = self.read_config_file(path)
        except FileNotFoundError:
            self._data = {}

    def read_config_file(self, path: Path) -> DatabaseType:
        with open(path) as file:
            content = file.read()
        return yaml.load(content, Loader=yaml.CLoader)

    def _show_key_path(self, *key_path: str) -> str:
        return '.'.join(key_path)

    def ask_key(self, required_type: type[ConfigValueType],
                     *key_path: str) -> ConfigValueType:
        try:
            return self._require_key(self._data, required_type, *key_path)
        except InvalidConfig:
            return self._require_key(self._default, required_type, *key_path)

    def require_key(self, required_type: type[ConfigValueType],
                         *key_path: str) -> ConfigValueType:
        return self._require_key(self._data, required_type, *key_path)

    def _require_key(self, start_database: DatabaseType,
                           required_type: type[ConfigValueType],
                           *key_path: str) -> ConfigValueType:
        database = start_database
        keypath_str = self._show_key_path(*key_path)
        error = InvalidConfig(f'Key "{keypath_str}" is required in config file')
        for key in key_path:
            if database is None:
                raise error
            try:
                database = database[key]
            except KeyError:
                raise error
        if isinstance(database, required_type):
            return database
        raise InvalidConfig(f'Key "{keypath_str}" should be of type {required_type}'
                            f' but has value "{database}"')

    def get_merged_dict(self, *key_path: str) -> DatabaseType:
        default_dict = self._require_key(self._default, dict, *key_path)
        user_dict = self.ask_key(dict, *key_path)
        return default_dict | user_dict

